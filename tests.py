from main import multipliers
import pytest


@pytest.mark.parametrize("i, n, res_str, res", [(10, 10, '', ' 1 2 5 10'),
                                                (1, 1, '', ' 1'),
                                                (100, 100, '', ' 1 2 4 5 10 20 25 50 100')])
def test_1(i, n, res_str, res):
    assert multipliers(n, i, res_str) == res, 'ЧЁТА не так'
