def values():
    n = input("Введите натуральное число большее 1: ")
    try:
        n = int(n)
    except:
        print("Введено некорректное число число")
    i = n
    res_str = ''
    print(multipliers(n, i, res_str))


def multipliers(n, i, res_str):
    try:
        if i > 0:
            res = n // i
            if n % i == 0:
                res_str = res_str + ' ' + str(res)
            i -= 1
            return multipliers(n, i, res_str)
        else:
            return res_str
    except:
        return res_str, "Переполнение стека"


if __name__ == '__main__':
    values()
